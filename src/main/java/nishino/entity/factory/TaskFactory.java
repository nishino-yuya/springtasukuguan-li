package nishino.entity.factory;

import org.springframework.stereotype.Component;

import nishino.dto.TaskDto;
import nishino.entity.Task;

@Component
public class TaskFactory {
	// dtoをentityに詰める
	public Task createEntity(TaskDto dto) {
		return new Task(
				dto.getId(),
				dto.getTaskName(),
				dto.getDiscription(),
				dto.getCategory(),
				dto.getChecked());
	}
}
