package nishino.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import nishino.dto.TaskDto;
import nishino.form.TopForm;
import nishino.service.TopService;

@Controller
public class TopController {
	@Autowired
	TopService topService;
	@RequestMapping(value = "/top", method = RequestMethod.GET)
	public String top(@ModelAttribute TopForm topForm, Model model) {
		TaskDto dto = new TaskDto();
		System.out.println(dto.getTaskName());
		System.out.println(dto.getCategory());
		return "top";
	}

	//modelにtaskを格納する
	@ModelAttribute("taskList")
	public List<TaskDto> showTasks(){
		return topService.showTasks();
	}
}
