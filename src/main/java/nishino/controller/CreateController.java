package nishino.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import nishino.dto.factory.TaskDtoFactory;
import nishino.form.TaskForm;
import nishino.service.CreateService;

@Controller
public class CreateController {
	@Autowired
	CreateService createService;
	@Autowired
	TaskDtoFactory taskDtoFactory;
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public String createTask(@ModelAttribute TaskForm taskForm, Model model) {
		model.addAttribute("taskForm", taskForm);
		return "create";
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String createTask(@ModelAttribute @Valid TaskForm taskForm, BindingResult result, Model model) {
		createService.registTask(taskDtoFactory.createDto(taskForm));
		return "redirect:top";
	}
}
