package nishino.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import nishino.form.TaskDeleteForm;
import nishino.service.DeleteTaskService;

@Controller
public class DeleteTaskController {
	@Autowired
	private DeleteTaskService deleteTaskService;
	@RequestMapping(value = "deleteTask", method = RequestMethod.POST)
	public String deleteTask(@ModelAttribute @Valid TaskDeleteForm taskDeleteForm) {
		deleteTaskService.deleteTask(taskDeleteForm.getId());
		return "redirect:top";
	}
}
