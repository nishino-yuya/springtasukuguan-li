package nishino.dto.factory;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import nishino.dto.TaskDto;
import nishino.entity.Task;
import nishino.form.TaskForm;

@Component
public class TaskDtoFactory {
	// create Dto from form
	public TaskDto createDto(TaskForm form) {
		return new TaskDto(
					null,
					form.getTaskName(),
					form.getDiscription(),
					form.getCategory(),
					form.getChecked());
	}

	// create Dto from entity
	public TaskDto createDto(Task entity) {
		return new TaskDto(
				entity.getId(),
				entity.getTaskName(),
				entity.getDiscription(),
				entity.getCategory(),
				entity.getChecked()
				);
	}

	// create DtoList from entityList
	public List<TaskDto> createDto(List<Task> entityList){
		List<TaskDto> dtoList = new ArrayList<TaskDto>();
		for(Task entity : entityList) {
			dtoList.add(createDto(entity));
		}
		return dtoList;
	}
}
