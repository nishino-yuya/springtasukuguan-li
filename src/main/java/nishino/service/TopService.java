package nishino.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nishino.dto.TaskDto;
import nishino.dto.factory.TaskDtoFactory;
import nishino.mapper.TasksMapper;

@Service
public class TopService {
	@Autowired
	TasksMapper tasksMapper;
	@Autowired
	TaskDtoFactory taskDtoFactory;
	public List<TaskDto> showTasks(){
		return taskDtoFactory.createDto(tasksMapper.showTasks());
	}
}
