package nishino.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nishino.mapper.TasksMapper;

@Service
public class DeleteTaskService {
	@Autowired
	private TasksMapper tasksMapper;
	public void deleteTask(Integer taskId) {
		tasksMapper.deleteTask(taskId);
	}
}
