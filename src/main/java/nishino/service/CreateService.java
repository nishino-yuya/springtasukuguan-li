package nishino.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nishino.dto.TaskDto;
import nishino.entity.factory.TaskFactory;
import nishino.mapper.TasksMapper;

@Service
public class CreateService {
	@Autowired
	TasksMapper tasksMapper;
	@Autowired
	TaskFactory taskFactory;
	public void registTask(TaskDto dto) {
		tasksMapper.createTask(taskFactory.createEntity(dto));
	}
}
