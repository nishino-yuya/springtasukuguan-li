package nishino.mapper;


import java.util.List;

import org.springframework.stereotype.Component;

import nishino.entity.Task;

@Component
public interface TasksMapper {
	void createTask(Task task);

	List<Task> showTasks();

	void deleteTask(Integer taskId);
}
