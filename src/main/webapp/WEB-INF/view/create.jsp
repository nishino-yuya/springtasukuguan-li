<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
	<meta charset="utf-8">
	<title>タスク新規登録</title>
</head>
	<body>
		<h2>タスク新規登録</h2>
		<form:form modelAttribute="taskForm">
			<label for="taskName">タスク名</label>
			<form:input path="taskName" />
			<label for="discription">詳細</label>
			<form:textarea path="discription" />
			<label for="category">カテゴリー</label>
			<form:input path="category" />
			<form:hidden path="checked" value="0" />
			<input type="submit" value="登録する">
		</form:form>
	</body>
</html>