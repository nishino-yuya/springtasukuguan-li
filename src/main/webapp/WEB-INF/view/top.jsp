<!DOCTYPE html>
	<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
	<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
	<head>
		<meta charset="UTF-8">
		<title>ホーム画面</title>
	</head>
<body>
	<a href="./create">新規タスク作成</a>
	<h2>タスク一覧</h2>
	<form:form action="/deleteTask" modelAttribute="taskDeleteForm" method="post">
	<c:forEach items="${taskList}" var="task">
		<c:out value="${task.taskName}" /><br>
		<c:out value="${task.discription}" /><br>
		<c:out value="${task.category}" /><br><br>
			<form:hidden path="deleteTask" value="${task.id}" />
			<input type="submit" value="削除する" >
	</c:forEach>
	</form:form>
</body>
</html>